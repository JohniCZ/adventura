package logika;

import logika.commands.IPrikaz;
import logika.commands.PrikazJdi;
import logika.core.HerniPlan;
import logika.core.Hra;
import logika.gameplaces.Prostor;
import logika.persons.Player;
import org.junit.Before;

public abstract class AbstractTest {

    protected Hra game;
    protected Player player;
    protected HerniPlan gamePlan;
    protected IPrikaz command;
    protected final String INVALID_NPC = "INVALID_NPC";
    protected final String INVALID_PLACE = "INVALID_PLACE";
    protected final String INVALID_ITEM = "INVALID_ITEM";
    private IPrikaz goCommand;

    @Before
    public void init() throws Exception {
        game = new Hra();
        player = Player.getInstance();
        gamePlan = game.getHerniPlan();
        goCommand = new PrikazJdi(gamePlan);
    }

    protected void goToInn() {
        goToField();

        goCommand.provedPrikaz("hospoda");
    }

    protected void goToCave() {
        goToField();
        goCommand.provedPrikaz("jeskyne");
    }

    protected void goToField() {
        goCommand.provedPrikaz("louka");
    }

    protected void goToFieldWithTree() {
        goToField();
        goCommand.provedPrikaz("mytina");
    }

    protected void goToHillWithTree() {
        goToFieldWithTree();
        goCommand.provedPrikaz("pahorek");
    }

    protected void goToLeanSeekerHouse() {
        goToInn();
        goCommand.provedPrikaz("nevestinec");
    }

    protected void goToAlchemist() {
        goToLeanSeekerHouse();
        goCommand.provedPrikaz("alchymista");
    }

    protected void goToChallangePlace() {
        goToLeanSeekerHouse();
        goCommand.provedPrikaz("Turnaj");
    }
}
