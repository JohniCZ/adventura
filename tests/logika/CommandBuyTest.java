package logika;

import logika.commands.CommandBuy;
import org.junit.Assert;
import org.junit.Test;

import static logika.Constants.ITEM_POTION;

public class CommandBuyTest extends AbstractTest {

    @Override
    public void init() throws Exception {
        super.init();
        command = new CommandBuy(gamePlan, game);
        goToAlchemist();
    }

    @Test
    public void testBuyElixir() {
        String text = command.provedPrikaz("Alchymista", ITEM_POTION);
        Assert.assertEquals("Děkuji ti za nákup.", text);
        Assert.assertNotNull(player.getInventory().getItem(ITEM_POTION));
        Assert.assertEquals(new Integer(200), player.getCoins());
    }

    @Test
    public void testInvalidNpc() {
        String text = command.provedPrikaz(INVALID_NPC);
        Assert.assertEquals("Nemůžeš nakupovat od někoho, kdo zde není.", text);
    }

    @Test
    public void testInvalidItemName() {
        String text = command.provedPrikaz("Alchymista", INVALID_ITEM);
        Assert.assertEquals("Bohužel, tento předmět u sebe nemám. Mám pouze " + ITEM_POTION + " ", text);
    }

    @Test
    public void testNoItem() {
        String text = command.provedPrikaz("Alchymista");
        Assert.assertEquals("Co mám koupit?", text);

    }
}
