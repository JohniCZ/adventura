package logika;

import logika.commands.CommandShowInventory;
import org.junit.Assert;
import org.junit.Test;

public class CommandTestShowInventory extends AbstractTest {

    @Override
    public void init() throws Exception {
        super.init();
        command = new CommandShowInventory();
    }

    @Test
    public void testShowInventory() {
        String text = command.provedPrikaz();
        Assert.assertEquals(player.getInventory().toString() + "\n Zlatých:" + player.getCoins(), text);
    }
}
