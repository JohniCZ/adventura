package logika;

import logika.commands.PrikazKonec;
import org.junit.Assert;
import org.junit.Test;

public class CommandEndTest extends AbstractTest {
    @Override
    public void init() throws Exception {
        super.init();
        command = new PrikazKonec(game);
    }

    @Test
    public void testEndGame() {
        String text = command.provedPrikaz();

        Assert.assertEquals("Hra ukončena příkazem konec.", text);
        Assert.assertTrue(game.konecHry());
    }
}
