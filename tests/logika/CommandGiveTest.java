package logika;

import logika.commands.CommandGive;
import logika.items.Vec;
import org.junit.Assert;
import org.junit.Test;

import static logika.Constants.ITEM_HERB;

public class CommandGiveTest extends AbstractTest {

    @Override
    public void init() throws Exception {
        super.init();
        command = new CommandGive(gamePlan, game);
        player.setCoins(0);
    }

    @Test
    public void testGiveMarigold() {
        Vec questItem = new Vec("Loutna");
        player.getInventory().addItem(questItem);
        String text = command.provedPrikaz("Marigold", "Loutna");
        Assert.assertEquals("O, to je moje loutna. Děkuji ti moc Geralde. \n Gratuluji, vyhrál jsi hru.", text);

        Assert.assertTrue(game.konecHry());
    }

    @Test
    public void testGiveInnMaster() {
        goToInn();
        Vec questItem = new Vec(ITEM_HERB);
        player.getInventory().addItem(questItem);
        String text = command.provedPrikaz("Hospodsky", ITEM_HERB);
        Assert.assertNull(player.getInventory().getItem("bylina"));

        Assert.assertEquals("To je přesně ta květina, kterou jsem chtěl. Mockrát ti děkuji. Tady je tvých 500 zlatých."
                + "\n Momentálně máš díky splnění úkolu " + player.getCoins() + " zlatých.", text);
        Assert.assertEquals(new Integer(500), player.getCoins());
    }

    @Test
    public void testGiveMoney() {
        goToLeanSeekerHouse();
        player.setCoins(1000);
        String text = command.provedPrikaz("Lichvar", "Penize");
        Assert.assertEquals("Na, tady máš tu loutnu. Řekni mu, ať se tu už neukazuje.", text);
        Assert.assertEquals(new Integer(0), player.getCoins());
        Assert.assertNotNull(player.getInventory().getItem("Loutna"));
    }
}
