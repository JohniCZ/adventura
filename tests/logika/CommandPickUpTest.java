package logika;

import logika.commands.CommandPickUp;
import org.junit.Assert;
import org.junit.Test;

public class CommandPickUpTest extends AbstractTest {

    @Override
    public void init() throws Exception {
        super.init();
        command = new CommandPickUp(gamePlan);
    }

    @Test
    public void testPickUp() {
        goToHillWithTree();

        String text = command.provedPrikaz("Bylina");
        Assert.assertEquals("Sebral jsi předmět Bylina.", text);
        Assert.assertNotNull(player.getInventory().getItem("Bylina"));
        Assert.assertTrue(gamePlan.getAktualniProstor().getVeci().isEmpty());
    }

    @Test
    public void testInvalidItem() {
        String text = command.provedPrikaz(INVALID_ITEM);
        Assert.assertEquals("Nemůžu sebrat, co neexistuje. Je tu pouze ", text);
    }
}
