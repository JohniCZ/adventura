package logika;

import logika.commands.PrikazJdi;
import org.junit.Assert;
import org.junit.Test;

public class CommandGoTest extends AbstractTest {

    @Override
    public void init() throws Exception {
        super.init();
        command = new PrikazJdi(gamePlan);
    }

    @Test
    public void testGo() {
        String text = command.provedPrikaz("Louka");
        Assert.assertEquals(gamePlan.getAktualniProstor().dlouhyPopis(), text);
        Assert.assertEquals("Louka", gamePlan.getAktualniProstor().getNazev());
    }

    @Test
    public void testInvalidTarget() {
        String text = command.provedPrikaz(INVALID_PLACE);
        Assert.assertEquals("Tam se odsud jít nedá! Můžeš jít do " + gamePlan.getAktualniProstor().getExitsNames(), text);
    }

    @Test
    public void testNoPlace() {
        String text = command.provedPrikaz();
        Assert.assertEquals("Kam mám jít? Musíš zadat jeden z východů:\n" + gamePlan.getAktualniProstor().getExitsNames(), text);
    }

}
