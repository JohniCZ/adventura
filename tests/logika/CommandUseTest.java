package logika;

import logika.commands.CommandUse;
import logika.items.Elixir;
import logika.items.Vec;
import org.junit.Assert;
import org.junit.Test;

import static logika.Constants.ITEM_POTION;

public class CommandUseTest extends AbstractTest {

    @Override
    public void init() throws Exception {
        super.init();
        command = new CommandUse();
        Elixir elixir = new Elixir(ITEM_POTION, 60);
        player.setHealth(20);
        player.getInventory().addItem(elixir);
    }

    @Test
    public void testUseItem() {
        String text = command.provedPrikaz(ITEM_POTION);
        Assert.assertEquals("Vypil jsi ozdravný elixír. Díky němu je tvoje zdraví na úrovni " + player.getHealth() + ".", text);
        Assert.assertNull(player.getInventory().getItem(ITEM_POTION));
        Assert.assertEquals(new Integer(80), player.getHealth());
    }

    @Test
    public void testInvalidItem() {
        String text = command.provedPrikaz(INVALID_ITEM);

        Assert.assertEquals("Předmět který chceš použít nemáš u sebe. Máš: " + player.getInventory().toString(), text);
        text = command.provedPrikaz();
        Assert.assertEquals("Co mám použít?", text);
    }

    @Test
    public void testUnusableItem() {
        player.getInventory().addItem(new Vec("TEST_ITEM"));
        String text = command.provedPrikaz("TEST_ITEM");
        Assert.assertEquals("Předmět který jsi zadal se nedá použít.", text);
    }
}
