package logika;

import logika.commands.CommandSpeak;
import logika.persons.Npc;
import org.junit.Assert;
import org.junit.Test;

import static logika.Constants.*;

public class CommandTestSpeak extends AbstractTest {

    @Override
    public void init() throws Exception {
        super.init();
        command = new CommandSpeak(gamePlan);
    }

    @Test
    public void speakWithMarigold() {
        String text = command.provedPrikaz("Marigold");
        Assert.assertEquals(SPACE_KAER_MORHEN, gamePlan.getAktualniProstor().getNazev());
        Assert.assertEquals(MARIGOLD_TEXT, text);
    }

    @Test
    public void testSpeakWithBandit() {
        goToFieldWithTree();
        String text = command.provedPrikaz("Bandita");
        Assert.assertEquals(BANDIT_TEXT, text);
    }

    @Test
    public void testSpeakWithInnMaster() {
        goToInn();
        System.out.println(gamePlan.getAktualniProstor().getNazev());
        String text = command.provedPrikaz("Hospodsky");
        Assert.assertEquals(INN_MASTER_TEXT, text);
    }

    @Test
    public void testSpeakWithGhoul() {
        goToCave();
        System.out.println(gamePlan.getAktualniProstor().getNazev());
        String text = command.provedPrikaz("Ghul");
        Assert.assertEquals(Npc.DEFAULT_TEXT, text);
    }

    @Test
    public void testSpeakWithAlchemist() {
        goToAlchemist();
        String text = command.provedPrikaz("Alchymista");
        Assert.assertEquals(ALCHEMIST_TEXT, text);
    }

    @Test
    public void testSpeakWithLoanSeeker() {
        goToLeanSeekerHouse();

        String text = command.provedPrikaz("Lichvar");
        Assert.assertEquals(MONEY_LENDER_TEXT, text);
    }

    @Test
    public void testSpeakWithWarriors() {
        goToChallangePlace();
        String text = command.provedPrikaz("BojovníkZacatecnik");
        Assert.assertEquals(SOLDIER_ONE_TEXT, text);
        text = command.provedPrikaz("PokrociliBojovnik");
        Assert.assertEquals(SOLDIER_TWO_TEXT, text);
        text = command.provedPrikaz("Sampion");
        Assert.assertEquals(CHAMPION_TEXT, text);
    }

    @Test
    public void testInvalidInput() {
        String text = command.provedPrikaz("INVALID_NAME");
        Assert.assertEquals("Zadaná postava INVALID_NAME neexistuje v momentálním prostoru.",text);
    }
}
