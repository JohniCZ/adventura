package logika;

import logika.commands.CommandFight;
import org.junit.Assert;
import org.junit.Test;

public class CommandCombatTest extends AbstractTest {
    @Override
    public void init() throws Exception {
        super.init();
        command = new CommandFight(gamePlan, game);
    }


    @Test
    public void testCannotFight() {
        goToInn();
        String texst = command.provedPrikaz("Hospodsky");
        Assert.assertEquals("S touto postavou nemůžeš bojovat.", texst);
    }

    @Test
    public void testInvalidNpc() {
        String text = command.provedPrikaz(INVALID_NPC);
        Assert.assertEquals("Postava jménem " + INVALID_NPC + " se zde nenachází.", text);
    }

    @Test
    public void testNoNpc() {
        goToField();
        String text = command.provedPrikaz(INVALID_NPC);
        Assert.assertEquals("Nemůžeš zaútočit, když tu nikdo není.", text);
    }
}
