package logika.commands;

import logika.persons.Player;

import static logika.Constants.CMD_INVENTORY;

/**
 * Třída CommandInventory - obstrarává příkaz inventar
 *
 * @author Jan Švejda
 * @version 1.0
 * @created květen 2019
 */

public class CommandShowInventory implements IPrikaz {

    private Player player;

    public CommandShowInventory() {
        this.player = Player.getInstance();
    }

    /**
     * Metoda vrátí string s výpisem inventáře a počtem zlatých.
     *
     * @param parametry nic nepřijímá
     * @return
     */

    @Override
    public String provedPrikaz(String... parametry) {
        return player.getInventory().toString() + "\n Zlatých:" + player.getCoins();
    }

    @Override
    public String getNazev() {
        return CMD_INVENTORY;
    }
}
