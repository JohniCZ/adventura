package logika.commands;

import logika.items.Elixir;
import logika.persons.Player;

import static logika.Constants.CMD_USE;
import static logika.Constants.ITEM_POTION;

/**
 * Třída CommandBuy - obstrarává příkaz koupit
 *
 * @author Jan Švejda
 * @version 1.0
 * @created květen 2019
 */

public class CommandUse implements IPrikaz {

    private Player player;


    public CommandUse() {
        this.player = Player.getInstance();
    }

    /**
     * Metoda zvaliduje vstupní parametry.
     * Provede kontrolu existence objektů v aktuální prostoru.
     * Zkontroluje, zda hráčův inventář obsahuje daný předmět.
     * Poté zjistí, zda se předmět dá použít a zavolá danou metodu pro použití.
     *
     * @param parametry parametry[0] = s kým mám mluvit
     * @return
     */


    @Override
    public String provedPrikaz(String... parametry) {
        if (parametry.length == 0) {
            return "Co mám použít?";
        }
        String returnString = "";
        String whatToUse = parametry[0];

        if (!player.getInventory().containItem(whatToUse)) {
            return "Předmět který chceš použít nemáš u sebe. Máš: " + player.getInventory().toString();
        }

        switch (whatToUse) {
            case ITEM_POTION:
                returnString = usePotion();
                break;
            default:
                returnString = "Předmět který jsi zadal se nedá použít.";
        }
        return returnString;
    }


    private String usePotion() {
        Elixir elixir = (Elixir) player.getInventory().getAndRemoveItem(ITEM_POTION);
        Integer newHp = player.getHealth() + elixir.getHealingPower();
        if (newHp > 100) {
            newHp = 100;
        }
        player.setHealth(newHp);
        return "Vypil jsi ozdravný elixír. Díky němu je tvoje zdraví na úrovni " + player.getHealth() + ".";
    }

    @Override
    public String getNazev() {
        return CMD_USE;
    }
}
