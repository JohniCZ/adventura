package logika.commands;

import logika.core.HerniPlan;
import logika.items.Vec;
import logika.persons.Player;

import static logika.Constants.CMD_PICK_UP;

/**
 * Třída CommandPickUp - obstrarává příkaz seber
 *
 * @author Jan Švejda
 * @version 1.0
 * @created květen 2019
 */

public class CommandPickUp implements IPrikaz {
    private HerniPlan gamePlan;
    private Player player;

    public CommandPickUp(HerniPlan gamePlan) {
        this.gamePlan = gamePlan;
        this.player = Player.getInstance();
    }


    /**
     * Metoda zvaliduje vstupní parametry.
     * Provede kontrolu existence objektů v aktuální prostoru.
     * Když předmět existuje, tak ho odstraní z prostoru a předá hráči do inventáře
     *
     * @param parametry parametry[0]= co zvedám
     * @return
     */
    @Override
    public String provedPrikaz(String... parametry) {
        if (parametry.length == 0) {
            return "Co mám sebrat?";
        }

        String whatToPick = parametry[0];

        if (gamePlan.getAktualniProstor().vecJeVProstoru(whatToPick)) {
            Vec item = gamePlan.getAktualniProstor().getVeci().get(whatToPick);
            gamePlan.getAktualniProstor().odstranVec(whatToPick);
            player.getInventory().addItem(item);
            return "Sebral jsi předmět " + item.getNazev() + ".";
        } else {
            return "Nemůžu sebrat, co neexistuje. Je tu pouze " + gamePlan.getAktualniProstor().getItemsToString();
        }

    }

    @Override
    public String getNazev() {
        return CMD_PICK_UP;
    }
}
