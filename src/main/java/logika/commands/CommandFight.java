package logika.commands;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import logika.core.Combat;
import logika.core.HerniPlan;
import logika.core.Hra;
import logika.gameplaces.Prostor;
import logika.persons.Npc;
import logika.persons.Player;

import static logika.Constants.CMD_FIGHT;

/**
 * Třída CommandFight - obstrarává příkaz boj
 *
 * @author Jan Švejda
 * @version 1.0
 * @created květen 2019
 */

public class CommandFight implements IPrikaz {

    private HerniPlan gamePlan;
    private Player player;
    private Combat combat;
    private Hra game;



    public CommandFight(HerniPlan gamePlan, Hra game) {
        this.gamePlan = gamePlan;
        this.player = Player.getInstance();
        this.game = game;
    }


    /**
     * Příkaz zkontroluje validitu vstupních hodnot.
     * Dále zkontroluje existenci v aktuálním prostoru.
     * Následně inicializuje třídu Combat, které předá Player,Npc,Hra,HerniPlan
     * Třída Combat provede samotný souboj a vrátí string s textem.
     *
     * @param parametry počet parametrů závisí na konkrétním příkazu.
     * @return text, který se vytiskne.
     */
    @Override
    public String provedPrikaz(String... parametry) {
        if (parametry.length == 0) {
            // pokud chybí druhé slovo (sousední prostor), tak ....
            return "Na koho mám zaútočit?";
        }

        Prostor position = gamePlan.getAktualniProstor();

        if (position.getPersons().size() == 0) {
            return "Nemůžeš zaútočit, když tu nikdo není.";
        }

        String npcName = parametry[0];
        if (gamePlan.getAktualniProstor().npcExists(npcName)) {
            Npc npc = gamePlan.getAktualniProstor().getNpc(npcName);
            if (npc.isHostile()) {
                combat = new Combat(player, npc, game, gamePlan);
                return combat.fightWithNpc();
            } else {
                return "S touto postavou nemůžeš bojovat.";

            }
        } else {
            return "Postava jménem " + npcName + " se zde nenachází.";

        }
    }


    @Override
    public String getNazev() {
        return CMD_FIGHT;
    }
}
