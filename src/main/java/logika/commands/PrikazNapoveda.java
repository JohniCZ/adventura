package logika.commands;

import logika.core.SeznamPrikazu;

import static logika.Constants.*;

/**
 * Třída PrikazNapoveda implementuje pro hru příkaz napoveda.
 * Tato třída je součástí jednoduché textové hry.
 *
 * @author Jarmila Pavlickova, Luboš Pavlíček,Jan Švejda
 * @version pro školní rok 2018/2019
 */
public class PrikazNapoveda implements IPrikaz {

    private SeznamPrikazu platnePrikazy;


    /**
     * Konstruktor třídy
     *
     * @param platnePrikazy seznam příkazů,
     *                      které je možné ve hře použít,
     *                      aby je nápověda mohla zobrazit uživateli.
     */
    public PrikazNapoveda(SeznamPrikazu platnePrikazy) {
        this.platnePrikazy = platnePrikazy;
    }


    /**
     * Vrací základní nápovědu po zadání příkazu "napoveda". Nyní se vypisuje
     * vcelku primitivní zpráva a seznam dostupných příkazů.
     *
     * @return napoveda ke hre
     */
    @Override
    public String provedPrikaz(String... parametry) {
        if (parametry.length > 0) {
            switch (parametry[0]) {
                case CMD_HELP:
                    return CMD_HELP + " \n " + CMD_HELP + " Příkaz";
                case CMD_END:
                    return CMD_END;
                case CMD_FIGHT:
                    return CMD_FIGHT + " cíl";
                case CMD_GIVE:
                    return CMD_GIVE + " Postava Předmět/Penize";
                case CMD_GO:
                    return CMD_GO + " Prostor";
                case CMD_INVENTORY:
                    return CMD_INVENTORY;
                case CMD_PICK_UP:
                    return CMD_PICK_UP + " Předmět";
                case CMD_SPEAK:
                    return CMD_SPEAK + " Postava";
                case CMD_USE:
                    return CMD_USE + " Předmět";
                case CMD_BUY:
                    return CMD_BUY + "Postava " + "Předmět";
                default:
                    return "Zadal jsi nápovědu pro neplatný příkaz " + parametry[0];
            }

        }

        return "Tvým úkolem je najít Marigoldovu ztracenou loutnu.\n"
                + "Pro nápovědu k příkazu zadej příkaz \" nápověda příkaz\"\n"
                + "\n"
                + "Můžeš zadat tyto příkazy:\n"
                + platnePrikazy.vratNazvyPrikazu();
    }

    /**
     * Metoda vrací název příkazu (slovo které používá hráč pro jeho vyvolání)
     *
     * @ return nazev prikazu
     */
    @Override
    public String getNazev() {
        return CMD_HELP;
    }

}
