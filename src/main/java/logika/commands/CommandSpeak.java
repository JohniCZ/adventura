package logika.commands;

import logika.core.HerniPlan;

import static logika.Constants.CMD_SPEAK;

/**
 * Třída CommandSpeak - obstrarává příkaz mluv
 *
 * @author Jan Švejda
 * @version 1.0
 * @created květen 2019
 */

public class CommandSpeak implements IPrikaz {

    private HerniPlan gamePlan;

    public CommandSpeak(HerniPlan gamePlan) {
        this.gamePlan = gamePlan;
    }

    /**
     * Metoda zvaliduje vstupní parametry.
     * Provede kontrolu existence objektů v aktuální prostoru.
     * Poté pomocí metody getTextToSpeech vrátí řetězec s textem.
     *
     * @param parametry parametry[0] = s kým mám mluvit
     * @return
     */

    @Override
    public String provedPrikaz(String... parametry) {
        if (parametry.length == 0) {
            return "S kým si mám promluvit?";
        }
        String withWhoSpeak = parametry[0];

        if (gamePlan.getAktualniProstor().npcExists(withWhoSpeak)) {
            return gamePlan.getAktualniProstor().getNpc(withWhoSpeak).getTextToSpeech();
        } else {
            return "Zadaná postava " + withWhoSpeak + " neexistuje v momentálním prostoru.";
        }
    }

    @Override
    public String getNazev() {
        return CMD_SPEAK;
    }
}
