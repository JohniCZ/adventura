package logika.commands;

import logika.core.HerniPlan;
import logika.core.Hra;
import logika.items.Vec;
import logika.persons.Npc;
import logika.persons.Player;

import static logika.Constants.*;

/**
 * Třída CommandGive - obstrarává příkaz dej
 *
 * @author Jan Švejda
 * @version 1.0
 * @created květen 2019
 */

public class CommandGive implements IPrikaz {

    private HerniPlan gamePlan;
    private Player player;
    private Hra hra;

    public CommandGive(HerniPlan gamePlan, Hra hra) {
        this.gamePlan = gamePlan;
        this.player = Player.getInstance();
        this.hra = hra;
    }

    /**
     * Metoda zvaliduje vstupní parametry.
     * Provede kontrolu existence objektů v aktuální prostoru.
     * Poté podle toho, co předává provede další příkazy.
     * Pokud se předávají peníze, tak se kontroluje, zda se předávají Lichváři, který je po nás chce.
     * Pokud nejsou lichvářovi, tak nám NPC odpoví, že je nechce.
     * <p>
     * Když se předává předmět, tak se kontroluje zda to je Loutna a zda se předává Marigoldovi.
     * Dále pokud je předmět Bylina, tak se kontroluje předání Hospodskému.
     * Ten dá za odměnu 500 zlatých.
     * Jinak si NPC předmět nechá a poděkuje.
     *
     * @param parametry parametry[0] komu co předávám, parametry[1] co předávám
     * @return Řetězec s textem
     */

    @Override
    public String provedPrikaz(String... parametry) {
        if (parametry.length == 0) {
            return "Co mám komu dát?";
        }

        String target = parametry[0];
        Npc toGive = null;
        for (Npc npc : gamePlan.getAktualniProstor().getPersons()) {
            if (npc.getName().equals(target)) {
                toGive = npc;
                break;
            }
        }
        if (toGive == null) {
            return "Npc neexistuje. Vyber prosím jedno z " + gamePlan.getAktualniProstor().getNpcsToString();
        }

        String whatToGive;

        try {
            whatToGive = parametry[1];
        } catch (ArrayIndexOutOfBoundsException e) {
            return "Co mám předat?";
        }

        if (whatToGive.equals(ITEM_MONEY)) {
            if (toGive.getName().equals(NPC_MONEY_LENDER)) {
                if (player.getCoins() >= 1000) {
                    player.getInventory().addItem(toGive.getInventory().getAndRemoveItem(ITEM_LUTE));
                    player.setCoins(player.getCoins() - 1000);
                    return "Na, tady máš tu loutnu. Řekni mu, ať se tu už neukazuje.";
                } else {
                    return "Nemáš dostatek peněz. Tak co tu okouníš a běž mi je sehnat.";
                }
            } else {
                return "Nech si svoje peníze.";
            }
        } else {
            Vec vec = player.getInventory().getAndRemoveItem(whatToGive);
            if (vec != null) {
                if (toGive.getName().equals(NPC_MARIGOLD) && vec.getNazev().equals(ITEM_LUTE)) {
                    hra.setKonecHry(true);
                    return "O, to je moje loutna. Děkuji ti moc Geralde. \n Gratuluji, vyhrál jsi hru.";

                } else if (toGive.getName().equals(NPC_INN_MASTER) && vec.getNazev().equals(ITEM_HERB)) {
                    player.getInventory().getAndRemoveItem("bylina");
                    player.setCoins(player.getCoins() + 500);
                    return "To je přesně ta květina, kterou jsem chtěl. Mockrát ti děkuji. Tady je tvých 500 zlatých."
                            + "\n Momentálně máš díky splnění úkolu " + player.getCoins() + " zlatých.";
                }
                return "Nevím proč mi to dáváš, ale vemu si to.";
            } else {
                return "Nemůžeš předat něco, co nemáš. Můžeš předat " + player.getInventory().toString() + " penize.";
            }
        }
    }

    @Override
    public String getNazev() {
        return CMD_GIVE;
    }
}
