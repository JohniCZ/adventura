package logika.commands;

import logika.core.Combat;
import logika.core.HerniPlan;
import logika.core.Hra;
import logika.items.Vec;
import logika.persons.Npc;
import logika.persons.Player;

import static logika.Constants.CMD_BUY;

/**
 * Třída CommandBuy - obstrarává příkaz koupit
 *
 * @author Jan Švejda
 * @version 1.0
 * @created květen 2019
 */

public class CommandBuy implements IPrikaz {

    HerniPlan gamePlan;
    Player player;
    Combat combat;
    Hra game;

    public CommandBuy(HerniPlan gamePlan, Hra game) {
        this.gamePlan = gamePlan;
        this.player = Player.getInstance();
        this.game = game;
    }

    /**
     * Metoda proved prikaz provede začáteční validaci, zda uživatel zadá potřebné paramatry.
     * Dále zkontroluje, zda zadané parametry existují v aktuálním prostoru.
     * Další krok je kontrola, zda cílové NPC má předmět, který se dá prodat. Detekce pomocí null hodnoty pro getPriceToBuy
     * Zkontroluje, zda má hráč dostatek peněz. Poté  vytvoří kopii předmětu co chce koupit a předá jí do hráčova inventáře.
     */
    @Override
    public String provedPrikaz(String... parametry) {
        if (parametry.length == 0) {
            return "Od koho a co mám koupit?";
        }
        String fromWho = parametry[0];

        if (gamePlan.getAktualniProstor().npcExists(fromWho)) {
            Npc npc = gamePlan.getAktualniProstor().getNpc(fromWho);

            if (npc.isHostile()) {
                combat = new Combat(player, npc, game, gamePlan);
                return combat.fightWithNpc();
            }
            String whatToBuy;
            try {
                whatToBuy = parametry[1];
            } catch (ArrayIndexOutOfBoundsException e) {
                return "Co mám koupit?";
            }

            if (npc.getInventory().containItem(whatToBuy)) {
                Vec item = npc.getInventory().getItem(whatToBuy);
                if (item.getPriceToBuy() == null) {
                    return item.getNazev() + " ti nemůžu prodat.";
                }

                if (item.getPriceToBuy() > player.getCoins()) {
                    return "Bohužel nemáš dostatek zlatých aby sis mohl koupit " + item.getNazev();
                }

                player.getInventory().addItem(item);
                player.setCoins(player.getCoins() - item.getPriceToBuy());
                return "Děkuji ti za nákup.";
            } else {
                return "Bohužel, tento předmět u sebe nemám. Mám pouze " + npc.getInventory().toString();
            }
        }
        return "Nemůžeš nakupovat od někoho, kdo zde není.";
    }

    @Override
    public String getNazev() {
        return CMD_BUY;
    }
}
