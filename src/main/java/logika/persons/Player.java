package logika.persons;

/**
 * Třída Player - třída samotného hráče
 * vytvořená pomocí návrhového vzoru Jedináček
 * Dědí od abstractní třídy LivinEntity
 *
 * @author Jan Švejda
 * @version 1.0
 * @created květen 2019
 */

public class Player extends LivingEntity {

    private static Player player;

    private Player() {

    }

    public static synchronized Player getInstance() {
        if (player == null) {
            player = new Player();
            player.setBaseDamage(40);
            player.setCoins(300);
        }
        return player;
    }
}
