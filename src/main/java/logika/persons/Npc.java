package logika.persons;

import java.util.List;
import java.util.Random;

/**
 * Třída Npc
 * Základ pro všechny nehráčské entity.
 *
 * @author Jan Švejda
 * @version 1.0
 * @created květen 2019
 */

public class Npc extends LivingEntity {

    public static final String DEFAULT_TEXT = "ghasdgas lkjfdsajjkl kjasflhdsjakl. Toto stvoření očividne nezvládne mluvit lidskou řečí.";

    private List<String> textToSpeechList;

    private boolean isHostile;

    public boolean isHostile() {
        return isHostile;
    }

    public void setHostile(boolean hostile) {
        isHostile = hostile;
    }

    public void setTextToSpeechList(List<String> textToSpeechList) {
        this.textToSpeechList = textToSpeechList;
    }

    public String getTextToSpeech() {
        if (textToSpeechList == null) {
            return DEFAULT_TEXT;
        }
        Random random = new Random();

        return textToSpeechList.get(random.nextInt(textToSpeechList.size()));
    }


}
