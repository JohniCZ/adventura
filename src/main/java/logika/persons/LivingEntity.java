package logika.persons;

import logika.items.Inventory;

import java.util.Objects;

/**
 * Třída LivingEntity
 * Abstractní třída, od které dědí každá postava, která se objevuje ve hře.
 *
 * @author Jan Švejda
 * @version 1.0
 * @created květen 2019
 */

public abstract class LivingEntity {

    private Integer health = 100;
    private String name;
    private Integer baseDamage = 0;
    private Inventory inventory = new Inventory();
    private Integer coins = 0;

    public Integer getHealth() {
        return health;
    }

    public void setHealth(Integer health) {
        this.health = health;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getBaseDamage() {
        return baseDamage;
    }

    public void setBaseDamage(Integer baseDamage) {
        this.baseDamage = baseDamage;
    }

    public Integer getCoins() {
        return coins;
    }

    public void setCoins(Integer coins) {
        this.coins = coins;
    }

    public Inventory getInventory() {
        return inventory;
    }

    public void setInventory(Inventory inventory) {
        this.inventory = inventory;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LivingEntity that = (LivingEntity) o;
        return Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
