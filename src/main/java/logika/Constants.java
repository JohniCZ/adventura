package logika;

/**
 * Třída Constants - obsahuje pomocná třída obsahuje konstanty
 *
 * @author Jan Švejda
 * @version 1.0
 * @created květen 2019
 */
public abstract class Constants {

    public static final String CMD_FIGHT = "bojuj";
    public static final String CMD_GIVE = "dej";
    public static final String CMD_PICK_UP = "seber";
    public static final String CMD_INVENTORY = "inventar";
    public static final String CMD_SPEAK = "mluv";
    public static final String CMD_USE = "pouzij";
    public static final String CMD_GO = "jdi";
    public static final String CMD_HELP = "napoveda";
    public static final String CMD_END = "konec";
    public static final String CMD_BUY = "kup";
    public static final String ITEM_LUTE = "Loutna";
    public static final String ITEM_MONEY = "Penize";
    public static final String ITEM_POTION = "Elixir";
    public static final String ITEM_HERB = "Bylina";
    public static final String NPC_MARIGOLD = "Marigold";
    public static final String NPC_MONEY_LENDER = "Lichvar";
    public static final String NPC_ALCHEMIST = "Alchymista";
    public static final String NPC_INN_MASTER = "Hospodsky";
    public static final String MARIGOLD_TEXT = "Geralde, už jsi našel mojí loutnu? Nevím, co bez ní budu dělat.";
    public static final String MONEY_LENDER_TEXT = "Ten zatracenec tu utratil 800 zlatých a utekl. Bohužel pro něj, tu zapoměl svojí loutnu. Za 1000 zlatých jsem ochotný ti jí vrátit.";
    public static final String BANDIT_TEXT = "Dej 200 zlatých, nebo tě zabiju.";
    public static final String INN_MASTER_TEXT = "Hledám vzácnou bylinu. Měla by být na pahorku pod stromem oběšenců.";
    public static final String SOLDIER_ONE_TEXT = "Tebe zabít bude lehký jako facka.";
    public static final String SOLDIER_TWO_TEXT = "Tak ty si myslíš, že mě dokážeš porazit?";
    public static final String CHAMPION_TEXT = "Ty? Ty se chceš stát šampionem? To těžko.";
    public static final String ALCHEMIST_TEXT = "Chceš si něco koupit? Vypadáš znavený. Nechceš koupit Elixir?";
    public static final String SPACE_KAER_MORHEN = "KaerMorhen";
    public static final String SPACE_FIELD = "Louka";
    public static final String SPACE_INN = "Hospoda";
    public static final String SPACE_CHALLANGE = "Turnaj";
    public static final String SPACE_CAVE = "Jeskyne";
    public static final String SPACE_FIELD_WITH_TREE = "";
    public static final String SPACE_ALCHEMIST = "Alchymista";
    public static final String SPACE_BOARD = "Vyvestnik";

    public static final String TEXT_WIN = "Gratuluji, vyhrál jsi. Chceš hrát znova?";
    public static final String TEXT_LOSE = "Prohral jsi. Chceš hrát znova?";

    public static final String CSS_SECTION_HEADER = "section-header";
}
