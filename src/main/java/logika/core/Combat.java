package logika.core;

import logika.items.Vec;
import logika.persons.Npc;
import logika.persons.Player;

import java.util.Random;
import java.util.Scanner;

/**
 * Třída Combat
 * Obstarává průběh boje mezi Hráčem a NPC.
 * Pokud hráč zemře, ukončí hru.
 * Jinak odebere NPC z prostoru a předá jeho předměty a peníze hráči.
 *
 * @author Jan Švejda
 * @version 1.0
 * @created květen 2019
 */

public class Combat {

    private Player player;
    private Npc npc;
    private HerniPlan gamePlan;
    private Hra game;

    /*
            Třída při inicializaci očekává, koho se bude souboj týkat.
     */

    public Combat(Player player, Npc npc, Hra game, HerniPlan gamePlan) {
        this.player = player;
        this.npc = npc;
        this.game = game;
        this.gamePlan = gamePlan;
    }


    /*
            Funkce provede a boj mezi hráčem a zadaným npc. Podmínka pro možnost boje je, že npc má True hodnotu isHostile.
            Dále vypisuje průběh boje.
            Pokud hráč prohraje, ukončí hru.
            Když hráč vyhraje, tak převezme předměty z inventáře NPC a jeho peníze.
            Následně NPC vymaže z prostoru.
     */
    public String fightWithNpc() {
        Random playerDmgRng = new Random();
        Random npcDmgRng = new Random();
        Random whoAttacksRng = new Random();

        while (player.getHealth() > 0 || npc.getHealth() > 0) {
            int playerDmg = playerDmgRng.nextInt(10) + player.getBaseDamage();
            int npcDmg = npcDmgRng.nextInt(5) + npc.getBaseDamage();
            boolean whoAttacks = whoAttacksRng.nextInt(100) > 50;
            //for TRUE NPC attacks first
            try {
                Thread.sleep(1000);
                if (whoAttacks) {
                    System.out.println(npc.getName() + " útočí první.");
                    System.out.println(npc.getName() + " dává poškození za " + npcDmg);

                    player.setHealth(player.getHealth() - npcDmg);

                    if (player.getHealth() <= 0) {
                        return playerLose();
                    }
                    System.out.println("Útočíš");
                    System.out.println("Dáváš poškození za " + playerDmg);
                    npc.setHealth(npc.getHealth() - playerDmg);

                    if (npc.getHealth() <= 0) {
                        return playerWin();
                    }
                } else {
                    System.out.println("Útočíš první.");
                    System.out.println("Dáváš poškození za " + npcDmg);
                    npc.setHealth(npc.getHealth() - playerDmg);

                    if (npc.getHealth() <= 0) {
                        return playerWin();
                    }

                    System.out.println(npc.getName() + " útočí.");
                    System.out.println(npc.getName() + " dává poškození za " + npcDmg);
                    player.setHealth(player.getHealth() - npcDmg);

                    if (player.getHealth() <= 0) {
                        return playerLose();
                    }
                }

                System.out.println("Aktuální život: " + player.getHealth());
                System.out.println("Aktuální život " + npc.getName() + ":" + npc.getHealth());
                System.out.println("______________");
            } catch (InterruptedException e) {
                System.err.println(e.getMessage());
            }

        }
        return "Chyba boje";
    }

    private String playerWin() {
        player.setCoins(player.getCoins() + npc.getCoins());

        for (Vec item : npc.getInventory().getInventoryArray()) {
            player.getInventory().addItem(item);
        }
        gamePlan.getAktualniProstor().removePerson(npc.getName());
        return "Zabil jsi svého soupeře a získal jsi " + npc.getCoins() + " zlatých a " + npc.getInventory();
    }

    private String playerLose() {
        game.setKonecHry(true);
        return "Byl jsi zabit postavou" + npc.getName();
    }
}
