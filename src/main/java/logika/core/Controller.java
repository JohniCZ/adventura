
package logika.core;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import logika.IHra;
import logika.gameplaces.Prostor;
import logika.items.Vec;
import logika.persons.Npc;
import logika.persons.Player;

import java.io.File;
import java.util.Map;

import static logika.Constants.*;

public class Controller {
    @FXML
    private VBox locationList;
    @FXML
    private VBox itemList;
    @FXML
    private VBox npcList;
    @FXML
    private VBox inventory;
    @FXML
    private Label healthLabel;
    @FXML
    private Label moneyLabel;

    @FXML
    private MenuItem newGameMenuItem;

    @FXML
    private MenuItem endGame;

    @FXML
    private MenuItem helpMenuItem;

    @FXML
    private VBox logger;

    private IHra game;

    public ImageView locationImage;

    /***************************************************************************
     * Metoda, která nastavý novou hru s GUI.
     *
     * @param game object nové hry
     */
    public void setGame(IHra game) {
        newGameMenuItem.setOnAction(event -> {
            this.setGame(new Hra());
        });

        endGame.setOnAction(event -> {
            System.exit(0);
        });

        helpMenuItem.setOnAction(event -> {
            this.showHelp();
        });
        this.game = game;
        showWhatsHappened(game.vratUvitani());
        changeSpace(game.getHerniPlan().getAktualniProstor());
        this.setHealthLabel();
        this.setMoneyLabel();
        this.setInventory();
    }

    /***************************************************************************
     * Metoda, která provede příkaz jdi a změni GUI.
     *
     * @param space object nového prostoru
     */
    private void changeSpace(Prostor space) {
        if (game.konecHry()) {
            gameEnded(TEXT_LOSE);
            return;
        }

        if (!space.getNazev().equals(game.getHerniPlan().getAktualniProstor().getNazev())) {
            showWhatsHappened(game.zpracujPrikaz(CMD_GO + " " + space.getNazev()));
        }

        String imageName = "/" + space.getNazev() + ".jpg";
        Image image;
        try {
            image = new Image(getClass().getResourceAsStream(imageName));
        } catch (NullPointerException e) {
            imageName = "/" + space.getNazev() + ".png";
            image = new Image(getClass().getResourceAsStream(imageName));
        }

        locationImage.setImage(image);
        setLocationList(space);
        setItemList(space);
        setNpcList(space);

    }

    /***************************************************************************
     * Metoda, která provede příkaz seber a změni GUI.
     *
     * @param item object předmětu v prostoru
     */
    private void pickUpItem(Vec item) {
        if (game.konecHry()) {
            gameEnded(TEXT_LOSE);
            return;
        }
        showWhatsHappened(game.zpracujPrikaz(CMD_PICK_UP + " " + item.getNazev()));
        this.setItemList(this.game.getHerniPlan().getAktualniProstor());
        this.setInventory();
    }

    /***************************************************************************
     * Metoda, která provede příkaz , podle stavu inventáře a NPC.
     *
     * @param npc postava, s kterou se bude interagovat
     */
    private void interactWithNpc(Npc npc) {
        if (game.konecHry()) {
            gameEnded(TEXT_LOSE);
            return;
        }
        if (npc.isHostile()) {
            this.showWhatsHappened(game.zpracujPrikaz(CMD_FIGHT + " " + npc.getName()));
            this.setNpcList(this.game.getHerniPlan().getAktualniProstor());
        } else {
            switch (npc.getName()) {
                case NPC_MARIGOLD: {
                    Vec vec = Player.getInstance().getInventory().getItem(ITEM_LUTE);
                    if (vec != null) {
                        this.showWhatsHappened(game.zpracujPrikaz(CMD_GIVE + " " + NPC_MARIGOLD + " " + vec.getNazev()));
                    } else {
                        this.showWhatsHappened(game.zpracujPrikaz(CMD_SPEAK + " " + npc.getName()));
                    }
                    break;
                }
                case NPC_MONEY_LENDER:
                    if (Player.getInstance().getCoins() >= 1000) {
                        this.showWhatsHappened(game.zpracujPrikaz(CMD_GIVE + " " + NPC_MONEY_LENDER + " " + ITEM_MONEY));
                    } else {
                        this.showWhatsHappened(game.zpracujPrikaz(CMD_SPEAK + " " + npc.getName()));
                    }
                    break;
                case NPC_ALCHEMIST: {
                    Vec vec = npc.getInventory().getItem(ITEM_POTION);
                    if (vec != null) {
                        this.showWhatsHappened(game.zpracujPrikaz(CMD_BUY + " " + npc.getName() + " " + ITEM_POTION));
                    } else {
                        this.showWhatsHappened(game.zpracujPrikaz(CMD_SPEAK + " " + npc.getName()));
                    }
                    break;
                }
                case NPC_INN_MASTER: {
                    Vec vec = Player.getInstance().getInventory().getItem(ITEM_HERB);
                    if (vec != null) {
                        this.showWhatsHappened(game.zpracujPrikaz(CMD_GIVE + " " + NPC_INN_MASTER + " " + ITEM_HERB));
                    } else {
                        this.showWhatsHappened(game.zpracujPrikaz(CMD_SPEAK + " " + npc.getName()));
                    }
                    break;
                }
                default:
                    this.showWhatsHappened(game.zpracujPrikaz(CMD_SPEAK + " " + npc.getName()));
                    break;
            }
        }
        this.setInventory();
        this.setHealthLabel();
        this.setMoneyLabel();

        if (this.game.konecHry()) {
            gameEnded(TEXT_WIN);
        }
    }

    /***************************************************************************
     * Metoda, která vypíše výpis s návratu od textové verze hry.
     *
     * @param log text v vypsání
     */
    private void showWhatsHappened(String log) {
        this.logger.getChildren().clear();
        this.logger.getChildren().add(new Label(log));
    }

    /***************************************************************************
     * Metoda, která přípraví GUI s okolními prostory aktuálního prostoru.
     *
     * @param space aktuální prostor
     */
    private void setLocationList(Prostor space) {
        locationList.getChildren().clear();

        HBox itemListHeader = new HBox();
        Label itemListHead = new Label();
        itemListHead.setText("Lokace:");
        itemListHead.getStyleClass().add(CSS_SECTION_HEADER);

        itemListHeader.getChildren().add(itemListHead);
        locationList.getChildren().add(itemListHeader);
        for (Prostor otherSpace : space.getVychody()) {
            HBox spaceBox = new HBox();
            spaceBox.setSpacing(10);

            Label spaceName = new Label();
            spaceName.setText(otherSpace.getNazev());
            ImageView spaceImageView = new ImageView();
            Image spaceImage;
            try {
                spaceImage = new Image(getClass().getClassLoader().getResourceAsStream("\\" + otherSpace.getNazev() + ".jpg"));
            } catch (NullPointerException e) {
                spaceImage = new Image(getClass().getClassLoader().getResourceAsStream("\\" + otherSpace.getNazev() + ".png"));
            }


            spaceImageView.setImage(spaceImage);
            spaceImageView.setFitHeight(40);
            spaceImageView.setFitWidth(40);

            spaceBox.getChildren().addAll(spaceImageView, spaceName);

            locationList.getChildren().add(spaceBox);
            spaceBox.setOnMouseClicked(event -> {
                changeSpace(otherSpace);
            });
        }
    }

    /***************************************************************************
     * Metoda, která přípraví GUI s NPC v prostoru.
     *
     * @param space aktuální prostor
     */
    private void setNpcList(Prostor space) {
        npcList.getChildren().clear();

        HBox itemListHeader = new HBox();
        Label itemListHead = new Label();
        itemListHead.setText("Postavy:");
        itemListHead.getStyleClass().add(CSS_SECTION_HEADER);

        itemListHeader.getChildren().add(itemListHead);
        npcList.getChildren().add(itemListHeader);

        for (Npc npc : space.getPersons()) {
            HBox npcBox = new HBox();
            npcBox.setSpacing(10);

            Label itemName = new Label();
            itemName.setText(npc.getName());


            npcBox.getChildren().addAll(itemName);

            npcList.getChildren().add(npcBox);
            npcBox.setOnMouseClicked(event -> {
                interactWithNpc(npc);
            });
        }
    }

    /***************************************************************************
     * Metoda, která přípraví GUI s předměty v prostoru.
     *
     * @param space aktuální prostor
     */
    private void setItemList(Prostor space) {
        itemList.getChildren().clear();
        HBox itemListHeader = new HBox();
        Label itemListHead = new Label();
        itemListHead.setText("Položené věci:");
        itemListHead.getStyleClass().add(CSS_SECTION_HEADER);

        itemListHeader.getChildren().add(itemListHead);
        itemList.getChildren().add(itemListHeader);
        for (Map.Entry<String, Vec> entry : space.getVeci().entrySet()) {
            Vec item = entry.getValue();
            HBox itemBox = new HBox();
            itemBox.setSpacing(10);

            Label itemName = new Label();
            itemName.setText(item.getNazev());


            ImageView itemImageView = new ImageView();
            Image itemImage;
            try {
                itemImage = new Image(getClass().getClassLoader().getResourceAsStream("\\" + item.getNazev() + ".jpg"));
            } catch (NullPointerException e) {
                itemImage = new Image(getClass().getClassLoader().getResourceAsStream("\\" + item.getNazev() + ".png"));
            }
            itemImageView.setImage(itemImage);
            itemImageView.setFitHeight(40);
            itemImageView.setFitWidth(40);


            itemBox.getChildren().addAll(itemImageView, itemName);

            itemList.getChildren().add(itemBox);
            itemBox.setOnMouseClicked(event -> {
                pickUpItem(item);
            });
        }
    }

    /***************************************************************************
     * Metoda, která přípraví GUI s předměty v inventáři.
     */
    private void setInventory() {
        inventory.getChildren().clear();

        HBox itemListHeader = new HBox();
        Label itemListHead = new Label();
        itemListHead.setText("Inventář:");
        itemListHead.getStyleClass().add(CSS_SECTION_HEADER);

        itemListHeader.getChildren().add(itemListHead);
        inventory.getChildren().add(itemListHeader);
        for (Vec item : Player.getInstance().getInventory().getInventoryArray()) {
            HBox itemBox = new HBox();
            itemBox.setSpacing(10);

            Label itemName = new Label();
            itemName.setText(item.getNazev());
            ImageView itemImageView = new ImageView();
            Image itemImage;
            try {
                itemImage = new Image(getClass().getClassLoader().getResourceAsStream("\\" + item.getNazev() + ".jpg"));
            } catch (NullPointerException e) {
                itemImage = new Image(getClass().getClassLoader().getResourceAsStream("\\" + item.getNazev() + ".png"));
            }
            itemImageView.setImage(itemImage);
            itemImageView.setFitHeight(40);
            itemImageView.setFitWidth(40);


            itemBox.getChildren().addAll(itemImageView, itemName);

            itemBox.setOnMouseClicked(event -> {
                useItem(item);
            });

            inventory.getChildren().add(itemBox);
        }
        this.setMoneyLabel();
    }

    /***************************************************************************
     * Metoda, která aktualizuje GUI s životy hráče.
     */
    private void setHealthLabel() {
        this.healthLabel.setText("Životy: " + Player.getInstance().getHealth().toString());
    }

    /***************************************************************************
     * Metoda, která použije předmět.
     *@param item předmět k použití
     */
    private void useItem(Vec item) {
        if (game.konecHry()) {
            gameEnded(TEXT_LOSE);
            return;
        }

        showWhatsHappened(game.zpracujPrikaz(CMD_USE + " " + item.getNazev()));
        this.setHealthLabel();


        this.setInventory();
        this.setItemList(game.getHerniPlan().getAktualniProstor());
    }

    /***************************************************************************
     * Metoda, která aktualizuje GUI s penězy hráče.
     */
    private void setMoneyLabel() {
        this.moneyLabel.setText("Peníze: " + Player.getInstance().getCoins().toString());
    }

    /***************************************************************************
     * Metoda, která ukáže nové GUI s možností ukončení hry, či začnutím znova.
     *
     */
    private void gameEnded(String text) {
        Label label = new Label();
        label.setText(text);
        HBox confirm = new HBox();
        Button confirmButton = new Button();
        confirmButton.setText("Ano");
        confirmButton.setOnMouseClicked(event -> {
            this.setGame(new Hra());
        });

        Button exitButton = new Button();
        exitButton.setText("Ne");
        exitButton.setOnMouseClicked(event -> {
            System.exit(0);
        });

        confirm.getChildren().addAll(confirmButton, exitButton);
        logger.getChildren().clear();
        logger.getChildren().addAll(label, confirm);
    }

    /***************************************************************************
     * Metoda, která ukáže nápovědu.
     *
     */
    private void showHelp() {
        WebView browser = new WebView();
        WebEngine webEngine = browser.getEngine();
        File f = new File(getClass().getClassLoader().getResource("Navod.html").getFile());
        webEngine.load(f.toURI().toString());
        this.logger.getChildren().clear();
        this.logger.getChildren().add(browser);
    }
}
