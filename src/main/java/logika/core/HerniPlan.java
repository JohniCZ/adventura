package logika.core;


import logika.gameplaces.Prostor;
import logika.items.Elixir;
import logika.items.Inventory;
import logika.items.Vec;
import logika.persons.Npc;

import java.util.Arrays;

import static logika.Constants.*;

/**
 * Class HerniPlan - třída představující mapu a stav adventury.
 * <p>
 * Tato třída inicializuje prvky ze kterých se hra skládá:
 * vytváří všechny prostory,
 * propojuje je vzájemně pomocí východů
 * a pamatuje si aktuální prostor, ve kterém se hráč právě nachází.
 *
 * @author Michael Kolling, Lubos Pavlicek, Jarmila Pavlickova
 * @version pro školní rok 2016/2017
 */
public class HerniPlan {

    private Prostor aktualniProstor;

    /**
     * Konstruktor který vytváří jednotlivé prostory a propojuje je pomocí východů.
     * Jako výchozí aktuální prostor nastaví halu.
     */
    public HerniPlan() {
        zalozProstoryHry();
    }

    /**
     * Vytváří jednotlivé prostory a propojuje je pomocí východů.
     * Jako výchozí aktuální prostor nastaví KhazModan.
     * Dále provede základní inicializaci všech postav.
     */
    private void zalozProstoryHry() {
        Prostor khazModan = new Prostor(SPACE_KAER_MORHEN, "Pevnost zaklínačů.");
        Prostor field = new Prostor(SPACE_FIELD, "Louka před Kaer Morhenem.");
        Prostor inn = new Prostor(SPACE_INN, "Hospoda která je na rozcestí.");
        Prostor challangePlace = new Prostor(SPACE_CHALLANGE, "Probíhá zde nějáký turnaj");
        Prostor cave = new Prostor(SPACE_CAVE, "Jeskyně. Podivně zapácha.");
        Prostor fieldWithTrees = new Prostor("Mytina", "Býval zde hustý les před válkou.");
        Prostor hillsBehindField = new Prostor("Pahorek", "Za války se zde staly hrozná zvěrstva. Strom, který tu je to dokazuje.");
        Prostor moneyLenderHouse = new Prostor("Nevestinec", "Je zde spousta krásných žen a ošlivých můžu.");
        Prostor alchemyShop = new Prostor(SPACE_ALCHEMIST, "Prodej různého zboží");
        Prostor vizimContractSpot = new Prostor(SPACE_BOARD, "Jsou zde různé zakázky. " +
                "Tebe ale zaujala zakázka za 500 zlatých na nalezení vzácné květiny na pahorku na mýtinou." +
                "Pro nálezce odměna čeká v hospodě.");

        khazModan.setVychod(field);

        field.setVychod(khazModan);
        field.setVychod(cave);
        field.setVychod(fieldWithTrees);
        field.setVychod(inn);

        inn.setVychod(field);
        inn.setVychod(challangePlace);
        inn.setVychod(moneyLenderHouse);

        challangePlace.setVychod(inn);
        challangePlace.setVychod(moneyLenderHouse);

        cave.setVychod(field);

        fieldWithTrees.setVychod(field);
        fieldWithTrees.setVychod(hillsBehindField);

        hillsBehindField.setVychod(fieldWithTrees);

        Vec bylina = new Vec("Bylina");
        hillsBehindField.pridejVec(bylina);

        moneyLenderHouse.setVychod(alchemyShop);
        moneyLenderHouse.setVychod(challangePlace);
        moneyLenderHouse.setVychod(inn);
        moneyLenderHouse.setVychod(vizimContractSpot);

        vizimContractSpot.setVychod(moneyLenderHouse);

        Npc marigold = new Npc();
        marigold.setHostile(false);
        marigold.setName(NPC_MARIGOLD);
        khazModan.addNpc(marigold);
        marigold.setTextToSpeechList(Arrays.asList(MARIGOLD_TEXT));

        Npc ghoul = new Npc();
        ghoul.setName("Ghul");
        ghoul.setHostile(true);
        ghoul.setBaseDamage(20);
        ghoul.setHealth(150);

        cave.addNpc(ghoul);

        Npc bandit = new Npc();
        bandit.setName("Bandita");
        bandit.setBaseDamage(10);
        bandit.setHostile(true);
        bandit.setCoins(200);
        Elixir elixir = new Elixir(ITEM_POTION, 60);
        Inventory banditInventory = new Inventory();
        banditInventory.addItem(elixir);
        bandit.setInventory(banditInventory);
        bandit.setTextToSpeechList(Arrays.asList(BANDIT_TEXT));

        fieldWithTrees.addNpc(bandit);

        Elixir elixirToBuy = new Elixir(ITEM_POTION, 60);
        elixirToBuy.setPriceToBuy(100);
        Npc alchemist = new Npc();
        alchemist.setCoins(500);
        alchemist.setHostile(false);
        alchemist.setName(NPC_ALCHEMIST);
        alchemist.getInventory().addItem(elixirToBuy);
        alchemist.setTextToSpeechList(Arrays.asList(ALCHEMIST_TEXT));

        alchemyShop.addNpc(alchemist);
        alchemyShop.setVychod(moneyLenderHouse);

        Npc innMaster = new Npc();
        innMaster.setName(NPC_INN_MASTER);
        innMaster.setCoins(500);
        innMaster.setTextToSpeechList(Arrays.asList(INN_MASTER_TEXT));
        inn.addNpc(innMaster);

        Npc moneyLender = new Npc();
        moneyLender.setName(NPC_MONEY_LENDER);

        Inventory moneyLenderInventory = moneyLender.getInventory();
        moneyLenderInventory.addItem(new Vec("Loutna"));
        moneyLender.setInventory(moneyLenderInventory);
        moneyLender.setTextToSpeechList(Arrays.asList(MONEY_LENDER_TEXT));

        moneyLenderHouse.addNpc(moneyLender);

        Npc fighterOne = new Npc();
        fighterOne.setName("BojovníkZacatecnik");
        fighterOne.setHostile(true);
        fighterOne.setBaseDamage(10);
        fighterOne.setCoins(300);
        fighterOne.setTextToSpeechList(Arrays.asList(SOLDIER_ONE_TEXT));

        Npc fighterTwo = new Npc();
        fighterTwo.setName("PokrociliBojovnik");
        fighterTwo.setHostile(true);
        fighterTwo.setBaseDamage(15);
        fighterTwo.setCoins(300);
        fighterTwo.setTextToSpeechList(Arrays.asList(SOLDIER_TWO_TEXT));

        Npc fighterThree = new Npc();
        fighterThree.setName("Sampion");
        fighterThree.setHostile(true);
        fighterThree.setBaseDamage(25);
        fighterThree.setCoins(400);

        fighterThree.setTextToSpeechList(Arrays.asList(CHAMPION_TEXT));

        challangePlace.addNpc(fighterOne);
        challangePlace.addNpc(fighterTwo);
        challangePlace.addNpc(fighterThree);
        aktualniProstor = khazModan;
    }

    /**
     * Metoda vrací odkaz na aktuální prostor, ve ktetém se hráč právě nachází.
     *
     * @return aktuální prostor
     */

    public Prostor getAktualniProstor() {
        return aktualniProstor;
    }

    /**
     * Metoda nastaví aktuální prostor, používá se nejčastěji při přechodu mezi prostory
     *
     * @param prostor nový aktuální prostor
     */
    public void setAktualniProstor(Prostor prostor) {
        aktualniProstor = prostor;
    }

}
