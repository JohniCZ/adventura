package logika.items;

/**
 * Třída Elixír
 * Dědí od třídy Věc.
 * Přidává navíc atribut healingPower, který určuje sílu uzdravení při použití.
 *
 * @author Jan Švejda
 * @version 1.0
 * @created květen 2019
 */

public class Elixir extends Vec {

    private Integer healingPower;

    public Elixir(String nazev, Integer healingPower) {
        super(nazev);
        this.healingPower = healingPower;
    }

    public Integer getHealingPower() {
        return healingPower;
    }


}
