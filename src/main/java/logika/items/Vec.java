package logika.items;

import java.util.Objects;

/**
 * Třída Vec
 * Třída od které dědí všechny věci.
 * Dává základní vlastnosti předmětů.
 *
 * @author Jan Švejda
 * @version 1.0
 * @created květen 2019
 */

public class Vec {

    private String nazev;

    private Integer priceToBuy;

    public Vec(String nazev) {
        this.nazev = nazev;
    }

    public Vec() {
    }

    public String getNazev() {
        return nazev;
    }

    public Integer getPriceToBuy() {
        return priceToBuy;
    }

    public void setPriceToBuy(Integer priceToBuy) {
        this.priceToBuy = priceToBuy;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Vec vec = (Vec) o;
        return Objects.equals(nazev, vec.nazev);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nazev);
    }
}
