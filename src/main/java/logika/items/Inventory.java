package logika.items;

import java.util.ArrayList;

/**
 * Třída Inventory
 * Obstrarává přenos předmětů pro všechny LivingEntity
 *
 * @author Jan Švejda
 * @version 1.0
 * @created květen 2019
 */

public class Inventory {

    private ArrayList<Vec> inventoryArray = new ArrayList<>(4);

    public Inventory() {

    }

    public Inventory(ArrayList<Vec> inventoryArray) {
        this.inventoryArray = inventoryArray;
    }

    public ArrayList<Vec> getInventoryArray() {
        return inventoryArray;
    }

    public void setInventoryArray(ArrayList<Vec> inventoryArray) {
        this.inventoryArray = inventoryArray;
    }

    public Vec getAndRemoveItem(String name) {
        Vec itemToReturn = null;
        for (Vec item : inventoryArray) {
            if (item.getNazev().equals(name)) {
                itemToReturn = item;
                inventoryArray.remove(item);
                break;
            }
        }
        return itemToReturn;
    }

    public boolean addItem(Vec item) {
        if (inventoryArray.contains(item)) {
            return false;
        } else {
            inventoryArray.add(item);
            return true;
        }
    }

    public boolean containItem(String itemName) {
        for (Vec item : inventoryArray) {
            if (item.getNazev().equals(itemName)) {
                return true;
            }
        }
        return false;
    }

    public Vec getItem(String name) {
        for (Vec item : inventoryArray) {
            if (item.getNazev().equals(name)) {
                return item;
            }
        }
        return null;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        if (inventoryArray.size() == 0) {
            return "Nic.";
        }
        for (Vec item : inventoryArray) {
            sb.append(item.getNazev());
            sb.append(" ");
        }
        return sb.toString();
    }
}
